list p=16F84A

status EQU 0x03
porta EQU 0x05
portb EQU 0x06
var_a EQU 0x0C

org 0x00
goto INICIO
org 0x05

INICIO

bsf status,5		;Banco 1
movlw 0x0F
movwf 0x05		;Inicializa el Puerto A como entrada
movlw 0x00
movwf 0x06		;Inicializa el Puerto B como salida
bcf status,5		;Banco 0

DECODIFICADOR

movf porta,0		;Mueve lo que se encuentra en el Puerto A hacia W
andlw 0x0F		;Hace una operacion AND de W vs 0x0F y lo guarda en W???
movwf var_a		;Mueve el contenido de W a var_a

UNO

decfsz var_a,1		;Decrementa a var_a y el resultado se vuelve a guardar en var_a
goto DOS		;Si el resultado del decremento NO es 0, pasa a checar el siguiente numero
movlw 0x06		;Si el resultado del decremento ES 0, mueve la literal a W
movwf portb		;Envia el contenido de W al portb
call TIEMPO		;Entra a la subrutina donde habra un retardo de tiempo
goto DECODIFICADOR	;Regresa a DECODIFICADOR para obtener un valor del porta y asi volver a comenzar


DOS

decfsz var_a,1
goto TRES
movlw 0x5B
movwf portb
call TIEMPO
goto DECODIFICADOR


TRES

decfsz var_a,1
goto CUATRO
movlw 0x4F
movwf portb
call TIEMPO
goto DECODIFICADOR


CUATRO

decfsz var_a,1
goto CINCO
movlw 0x66
movwf portb
call TIEMPO
goto DECODIFICADOR


CINCO

decfsz var_a,1
goto SEIS
movlw 0x6D
movwf portb
call TIEMPO
goto DECODIFICADOR


SEIS

decfsz var_a,1
goto SIETE
movlw 0x7D
movwf portb
call TIEMPO
goto DECODIFICADOR


SIETE

decfsz var_a,1
goto OCHO
movlw 0x47
movwf portb
call TIEMPO
goto DECODIFICADOR


OCHO

decfsz var_a,1
goto NUEVE
movlw 0x7F
movwf portb
call TIEMPO
goto DECODIFICADOR


NUEVE

decfsz var_a,1
goto DIEZ
movlw 0x67
movwf portb
call TIEMPO
goto DECODIFICADOR


DIEZ

decfsz var_a,1
goto ONCE
movlw 0x77
movwf portb
call TIEMPO
goto DECODIFICADOR


ONCE

decfsz var_a,1
goto DOCE
movlw 0x7C
movwf portb
call TIEMPO
goto DECODIFICADOR


DOCE

decfsz var_a,1
goto TRECE
movlw 0x59
movwf portb
call TIEMPO
goto DECODIFICADOR


TRECE

decfsz var_a,1
goto CATORCE
movlw 0x46
movwf portb
call TIEMPO
goto DECODIFICADOR


CATORCE

decfsz var_a,1
goto QUINCE
movlw 0x79
movwf portb
call TIEMPO
goto DECODIFICADOR


QUINCE

decfsz var_a,1
goto CERO
movlw 0x71
movwf portb
call TIEMPO
goto DECODIFICADOR


CERO

movlw 0x3F
movwf portb
call TIEMPO
goto DECODIFICADOR


TIEMPO      		; Esta subrutina, esta temporizada a 1s
    movlw   0x28
    movwf   0x0D	;Mueve un 0x28 a 0x0D

CLEAR
    bcf     0x0b, 2	;Limpia la bandera del TMR0 Overflow
    movlw   0x3d
    movwf   0x01	;Mueve un 0x3d al TMR0

INT
    btfss   0x0b, 2	;Verifica que la bandera del TMR0 Overflow se levante
    goto    INT		;Si la bandera NO se ha levantado, vuelve a verificar
    decfsz  0x0D, 1	;Si la bandera se levanto, decrementa lo que hay en 0x0D y lo guarda ahi
    goto CLEAR		;Si el decemento NO es 0, vuelve a CLEAR
    return		;Si el decremento ES 0, termina la subrutina

end


