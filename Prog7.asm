    list p=16F84A
    org 0x00
    goto INICIO
    org 0x05

INICIO
    bsf     0x03, 5
    movlw   0x07
    movwf   0x05
    clrf    0x06
    bcf     0x03, 5

LEER
    movlw   0x77    ;A(pagado)
    movwf   0x06

    btfsc   0x05, 0
    goto    LAMPARA
    btfsc   0x05, 1
    goto    TIMBRE
    btfsc   0x05, 2
    goto    VENTILADOR

    goto    LEER


LAMPARA
    bsf     0x06, 7
    movlw   0x73    ;P(rendido)
    movwf   0x06
    movlw   0x0F
    movwf   0x0C

QUINCE
    call    TIEMPO
    decfsz  0x0C
    goto    QUINCE

    bcf     0x06, 7
    goto    LEER


TIMBRE
    bsf     0x05, 3
    movlw   0x73    ;P(rendido)
    movwf   0x06

TIMBREP
    btfsc   0x05, 1
    goto    TIMBREP
    bcf     0x05, 3
    goto    LEER


VENTILADOR
    bsf     0x05, 4
    movlw   0x73    ;P(rendido)
    movwf   0x06

CONTINUA
    btfss   0x05, 2
    goto    CONTINUA

    bcf     0x05, 3
    goto    LEER


TIEMPO
    bsf 	0x03, 5
    movlw 	0x07
    movwf 	0x01
    bcf 	0x03, 5

    movlw   0x14
    movwf   0x0D

CLEAR
    bcf     0x0B, 2
    movlw   0x3D
    movwf   0x01

FLAG
    btfss   0x0B, 2
    goto    FLAG
    decfsz  0x0D, 1
    goto CLEAR
    return

end