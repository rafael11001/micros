list p=16F84A

status EQU 0x03
porta EQU 0x05
portb EQU 0x06
var_a EQU 0x0C

org 0x00
goto INICIO
org 0x05

INICIO
    bsf status,5		;Banco 1
    movlw 0x0F
    movwf 0x05		;Inicializa el Puerto A como entrada
    movlw 0x00
    movwf 0x06		;Inicializa el Puerto B como salida
    bcf status,5		;Banco 0

DECODIFICADOR
    movf porta,0		;Mueve lo que se encuentra en el Puerto A hacia W
    andlw 0x0F		;Hace una operacion AND de W vs 0x0F y lo guarda en W, para limitar la entrada a 4 bits
    call DISPLAY
    movwf portb     ; Envia el resultado de DISPLAY al puerto B
    goto DECODIFICADOR

; Start: Roldan Style code
DISPLAY
    addwf   0x02, 1
    retlw   0x3F    ; 0
    retlw   0x06    ; 1
    retlw   0x5B    ; 2
    retlw   0x4F    ; 3
    retlw   0x66    ; 4
    retlw   0x6D    ; 5
    retlw   0x7D    ; 6
    retlw   0x47    ; 7
    retlw   0x7F    ; 8
    retlw   0x67    ; 9
    retlw   0x77    ; A
    retlw   0x7C    ; B
    retlw   0x39    ; C
    retlw   0x46    ; D
    retlw   0x79    ; E
    retlw   0x71    ; F

; End: Roldan Style code

end
