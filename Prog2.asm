    list p=16F84A
	org 0x00
	goto INICIO
	org 0x05

INICIO
    bsf     0x03, 5
    movlw   0x07
    movwf   0x01
    clrf    0x06
    bcf     0x03, 5

NUEVO
    movlw   0x5e ;D
    movwf   0x06
    call TIEMPO

    movlw   0x06 ;I
    movwf   0x06
    call TIEMPO

    movlw   0x79 ;E
    movwf   0x06
    call TIEMPO

    movlw   0x7d ;G
    movwf   0x06
    call TIEMPO

    movlw   0x3f ;O
    movwf   0x06
    call TIEMPO

    movlw   0x00 ;_
    movwf   0x06
    call TIEMPO

    movlw   0x76 ;K
    movwf   0x06
    call TIEMPO

    movlw   0x79 ;E
    movwf   0x06
    call TIEMPO

    movlw   0x54 ;N
    movwf   0x06
    call TIEMPO

    movlw   0x54 ;N
    movwf   0x06
    call TIEMPO

    movlw   0x79 ;E
    movwf   0x06
    call TIEMPO

    movlw   0x31 ;T
    movwf   0x06
    call TIEMPO

    movlw   0x76 ;H
    movwf   0x06
    call TIEMPO

    movlw   0x00 ;_
    movwf   0x06
    call TIEMPO

    movlw   0x50 ;R
    movwf   0x06
    call TIEMPO

    movlw   0x77 ;A
    movwf   0x06
    call TIEMPO

    movlw   0x71 ;F
    movwf   0x06
    call TIEMPO

    movlw   0x77 ;A
    movwf   0x06
    call TIEMPO

    movlw   0x79 ;E
    movwf   0x06
    call TIEMPO

    movlw   0x38 ;L
    movwf   0x06
    call TIEMPO

    movlw   0x00 ;_
    movwf   0x06
    call TIEMPO

    movlw   0x3F ;0
    movwf   0x06
    call TIEMPO

    movlw   0x30 ;1
    movwf   0x06
    call TIEMPO
    
    movlw   0x5B ;2
    movwf   0x06
    call TIEMPO

    movlw   0x4F ;3
    movwf   0x06
    call TIEMPO

    movlw   0x66 ;4
    movwf   0x06
    call TIEMPO

    movlw   0x6D ;5
    movwf   0x06
    call TIEMPO

    movlw   0x7D ;6
    movwf   0x06
    call TIEMPO

    movlw   0x47 ;7
    movwf   0x06
    call TIEMPO

    movlw   0x7F ;8
    movwf   0x06
    call TIEMPO

    movlw   0x67 ;9
    movwf   0x06
    call TIEMPO

    movlw   0x77 ;A
    movwf   0x06
    call TIEMPO

    movlw   0x7c ;B
    movwf   0x06
    call TIEMPO

    movlw   0x39 ;C
    movwf   0x06
    call TIEMPO

    movlw   0x5E ;D
    movwf   0x06
    call TIEMPO

    movlw   0x79 ;E
    movwf   0x06
    call TIEMPO

    movlw   0x71 ;F
    movwf   0x06
    call TIEMPO
	goto NUEVO


TIEMPO				;Tiempo a ~1s
    movlw   0x28
    movwf   0x0D

CLEAR
    bcf     0x0b, 2
    movlw   0x3d
    movwf   0x01

INT
    btfss   0x0b, 2
    goto    INT
    decfsz  0x0D, 1
    goto CLEAR
    return

end