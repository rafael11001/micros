    list p=16F84A

    status EQU 0x03
    portb EQU 0x06

    org 0x00
    goto INICIO
    org 0x05

INICIO

    bsf status,5		;Banco 1
    movlw 0x00
    movwf 0x06		;Inicializa el Puerto B como salida
    bcf status,5		;Banco 0


INISECUENCIA

    movlw 0x80
    movwf portb		;Enciende el led con peso 128
    call TIEMPO		;Entra a la subrutina para hacer un retardo de tiempo


DERCOMPLETO

    rrf portb,1		;Rota los bits hacia la derecha, encendiendo el siguiente led
    call TIEMPO		;Entra a la subrutina para hacer un retardo de tiempo
    btfss portb,0		;Verifica que el bit peso 0 sea 1
    goto DERCOMPLETO	;Si el bit peso 0 NO es 1, regresa a DERCOMPLETO. Si ES 1, pasa a IZQCOMPLETO
    goto INISECUENCIA	;Si el bit peso 0 ES 1


TIEMPO      		; Esta subrutina, esta temporizada a 1s
    bsf     0x03, 5
    movlw   0x07
    movwf   0x01
    bcf     0x03, 5

    movlw   0x14
    movwf   0x0D	;Mueve un 0x28 a 0x0D

CLEAR
    bcf     0x0b, 2	;Limpia la bandera del TMR0 Overflow
    movlw   0x3d
    movwf   0x01	;Mueve un 0x3d al TMR0

INT
    btfss   0x0b, 2	;Verifica que la bandera del TMR0 Overflow se levante
    goto    INT		;Si la bandera NO se ha levantado, vuelve a verificar
    decfsz  0x0D, 1	;Si la bandera se levanto, decrementa lo que hay en 0x0D y lo guarda ahi
    goto CLEAR		;Si el decemento NO es 0, vuelve a CLEAR
    return		;Si el decremento ES 0, termina la subrutina

end
